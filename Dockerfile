FROM debian:latest
MAINTAINER rasch <me@randyschneck.com>
RUN apt-get update && apt-get install -y rtorrent && \
  rm -rf /var/lib/apt/lists/* && \
  useradd -ms /bin/bash torrent
USER torrent
WORKDIR /home/torrent
RUN mkdir -p session downloads
COPY .rtorrent.rc .rtorrent.rc
CMD ["/usr/bin/rtorrent"]
