dockertorrent
-------------
run rtorrent in a docker container

usage

    docker run -it --rm -v $(pwd):/home/torrent/downloads rasch/dockertorrent

this docker image currently works interactively. torrents can be added using
the rtorrent interface or by downloading .torrent files into the `pwd`. in the
future, the `.rtorrent.rc` file will be modified for a proper daemon mode
container.
